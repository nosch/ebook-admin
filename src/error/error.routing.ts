import {ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {ErrorComponent}  from './../error/error.component';

const errorRoutes: Routes = [
    {path: "**", component: ErrorComponent},
    {path: "error", component: ErrorComponent}
];

export const errorRouting: ModuleWithProviders = RouterModule.forChild(errorRoutes);
