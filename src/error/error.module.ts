import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {ErrorComponent} from "./error.component"

import {errorRouting} from "./error.routing";

@NgModule({
    imports: [
        CommonModule,
        errorRouting
    ],
    declarations: [
        ErrorComponent
    ],
    exports: [
        ErrorComponent
    ],
    providers: []
})
export class ErrorModule {}
