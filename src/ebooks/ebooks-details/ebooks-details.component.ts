import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {FormGroup, FormBuilder} from "@angular/forms";

import {Observable, Observer} from "rxjs";

import {AppModalService} from "./../../app/services/app-modal.service";
import {EbooksService} from "./../services/ebooks.service";
import {IEbook} from "./../ebooks.interface";

@Component({
    selector: "hgv-ebooks-details",
    templateUrl: "./ebooks-details.component.html",
    styles: [
        ".mdl-textfield, .mdl-selectfield {width: 100%}",
        "button {width: 100%}",
        "section {margin-bottom: 10px}",
        "section > header {display: flex; align-items: center; justify-content: center; min-height: 150px}",
        ".mdl-card__supporting-text {display: flex; align-items: center; justify-content: center; min-height: inherit}",
        ".cover-label {color: rgb(63,81,181)}"
    ]
})
export class EbooksDetailsComponent implements OnInit {
    public detailsForm: FormGroup;
    public formData: any;
    public ebookPublishers: Array<string>;
    public ebookCurrencies: Array<string>;
    public ebook: IEbook;

    public pending: boolean = false;
    public error: boolean = false;

    private ebookCoverData: File;
    private confirmCancelObserver: Observer<boolean> = null;

    private submitted: boolean = false;
    private deleted: boolean = false;

    constructor(private router: Router,
                private activeRoute: ActivatedRoute,
                private modalService: AppModalService,
                private ebooksService: EbooksService,
                private formBuilder: FormBuilder) {}

    ngOnInit(): void {
        // Get ebook details from route::resolve
        this.activeRoute.data
            .subscribe((result: {details: any}) => {
                this.ebook = result.details[0];
                this.ebookPublishers = result.details[1];
                this.ebookCurrencies = result.details[2];
            });

        this.buildForm();
        this.subcribeToChanges();
    }

    onCancel(): void {
        this.pending = true;
        this.gotoList();
    }

    onSave(): void {
        this.pending = true;
        this.submitted = true;
        this.mapFormData();
        this.saveEbook(this.ebook, this.ebookCoverData);
    }

    onDelete(): void {
        this.pending = true;
        this.deleted = true;

        this.modalService.confirm({
            title: "eBook löschen?",
            cancelButtonText: "Abbrechen",
            confirmButtonText: "Löschen"
        }, (confirmed: boolean) => {
            this.pending = confirmed;
            this.deleted = confirmed;

            if (confirmed) {
                this.deleteEbook(+this.activeRoute.snapshot.params['id']);
            }
        });
    }

    onSelectEbookCover($event: any): void {
        this.ebookCoverData = $event.fileData;
        this.detailsForm.controls["coverUrl"].markAsDirty();
    }

    onResetEbookCover(): void {
        this.ebookCoverData = null;
        this.detailsForm.controls["coverUrl"].markAsPristine();
    }

    /**
     * Called via ConfirmDeactivateGuard
     */
    confirmCancel(): Observable<boolean> | boolean {
        if (!this.error && this.hasChanges()) {
            this.modalService.confirm({
                title: "Änderungen verwerfen?",
                cancelButtonText: "Beibehalten",
                confirmButtonText: "Verwerfen"
            }, (confirmed: boolean) => {
                this.pending = confirmed;
                this.confirmCancelObserver.next(confirmed);
                this.confirmCancelObserver.complete();
            });

            return new Observable<boolean>((sender: Observer<boolean>) => {
                this.confirmCancelObserver = sender;
            });
        }

        return true;
    }

    hasChanges(): boolean {
        return this.detailsForm.dirty && !this.submitted && !this.deleted;
    }

    private gotoList(): void {
        this.router.navigate(["/ebooks"]);
    }

    private saveEbook(ebook: IEbook, ebookCover?: File): void {
        const ebookStream$ = (!ebook.id)
            ? (ebookCover)
                ? this.ebooksService.createEbookAndCover(ebook, ebookCover)
                : this.ebooksService.create(ebook)
            : (ebookCover)
                ? this.ebooksService.updateEbookAndCover(ebook, ebookCover)
                : this.ebooksService.update(ebook);

        ebookStream$.subscribe(
            () => this.gotoList(),
            () => {
                this.error = true;
                this.router.navigate(['/error'])
            }
        );
    };

    private deleteEbook(id: number): void {
        this.ebooksService.remove(id)
            .subscribe(
                () => this.gotoList(),
                () => {
                    this.error = true;
                    this.router.navigate(['/error'])
                }
            );
    }

    private subcribeToChanges() {
        const valueChanges$ = this.detailsForm.valueChanges;

        valueChanges$
            .subscribe((formData) => this.formData = formData);
    }

    private buildForm(): void {
        this.detailsForm = this.formBuilder.group({
            "title": [this.ebook.title],
            "author": [this.ebook.author],
            "publisher": [this.ebook.publisher],
            "isbn": [this.ebook.isbn],
            "price": [this.ebook.price],
            "currency": [this.ebook.currency],
            "coverUrl": [this.ebook.coverUrl]
        });
    }

    private mapFormData(): void {
        this.ebook.title = this.formData.title;
        this.ebook.author = this.formData.author;
        this.ebook.publisher = this.formData.publisher;
        this.ebook.isbn = this.formData.isbn;
        this.ebook.price = this.formData.price;
        this.ebook.currency = this.formData.currency;
    }
}
