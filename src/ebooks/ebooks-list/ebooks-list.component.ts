import {Component, OnInit} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";

import {IEbook} from "./../ebooks.interface";

@Component({
    selector: "hgv-ebooks-list",
    templateUrl: "./ebooks-list.component.html",
    styles: [
        "section {margin-bottom: 10px}",
        "section > header {display: flex; align-items: center; justify-content: center; min-height: 150px}"
    ]
})
export class EbooksListComponent implements OnInit {
    public ebooks: Array<IEbook> = [];

    constructor(private router: Router,
                private activeRoute: ActivatedRoute) {}

    ngOnInit(): void {
        // Get ebooks list from route::resolve
        this.activeRoute.data
            .subscribe((result: {list: Array<IEbook>}) => this.ebooks = result.list);
    }

    onSelect(ebook: IEbook): void {
        this.router.navigate(["/ebooks", ebook.id]);
    }
}
