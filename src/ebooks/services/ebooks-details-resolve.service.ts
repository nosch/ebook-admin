import {Injectable } from "@angular/core";
import {Router, Resolve, ActivatedRouteSnapshot} from "@angular/router";
import {Observable, Observer} from "rxjs";

import {EbooksService} from "./ebooks.service";
import {Ebook} from "./../ebook";
import {IEbook} from "./../ebooks.interface";

@Injectable()
export class EbooksDetailsResolve implements Resolve<IEbook> {
    private streams: Array<any> = [];
    
    constructor(private router: Router,
                private ebooksService: EbooksService) {}
    
    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        const ebookStream$ = (route.data["mode"] === "edit")
            ? this.ebooksService.getById(+route.params["id"])
            : Observable.create((observer: Observer<IEbook>) => {
                observer.next(new Ebook({}));
                observer.complete();
            });

        this.streams = [
            ebookStream$,
            this.ebooksService.getPublishers(),
            this.ebooksService.getCurrencies()
        ];

        return Observable.forkJoin(this.streams)
            .catch(() => this.router.navigate(["/error"]));
    }
}
