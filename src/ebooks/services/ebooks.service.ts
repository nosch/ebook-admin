import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

import {Observable, Observer} from 'rxjs';
import {ErrorObservable} from "rxjs/observable/ErrorObservable";

import {IEbook} from "../ebooks.interface";

export interface IEbooksService {
    get(): Observable<Array<IEbook>>;

    getById(id: number): Observable<IEbook>;

    create(ebook: IEbook): Observable<IEbook>;

    createEbookAndCover(ebook: IEbook, ebookCover: File): Observable<IEbook>;

    update(ebook: IEbook): Observable<IEbook>;

    updateEbookAndCover(ebook: IEbook, ebookCover: File): Observable<IEbook>;

    remove(id: number): Observable<Response>;

    getCoverById(id: number): Observable<string>;

    getPublishers(): Observable<Array<string>>;

    getCurrencies(): Observable<Array<string>>;
}

@Injectable()
export class EbooksService implements IEbooksService{
    constructor(private xhr: Http) {}

    private ebooksApi = "/api/ebooks";
    private ebooksCoversApi = "/api/covers";
    private publishersApi = "/api/publishers";
    private currenciesApi = "/api/currencies";

    get(): Observable<Array<IEbook>> {
        return this.xhr.get(this.ebooksApi)
            .map((response: Response) => response.json() || [])
            .flatMap((ebooks: Array<IEbook>) => ebooks)
            .flatMap(
                (ebook: IEbook) => ebook.coverUrl ? this.getCoverById(ebook.id) : Observable.of(""),
                (ebook: IEbook, response: string) => {
                    ebook.coverData = response;
                    return ebook;
                }
            )
            .toArray()
            .catch(this.handleError);
    }

    getById(id: number): Observable<IEbook> {
        let url = `${this.ebooksApi}/${id}`;

        return this.xhr.get(url)
            .map((response: Response) => response.json() || {})
            .flatMap((ebook: IEbook) => {
                return Observable.forkJoin(
                    Observable.of(ebook),
                    ebook.coverUrl ? this.getCoverById(ebook.id) : Observable.of("")
                );
            })
            .map((ebookWithCover) => {
                ebookWithCover[0].coverData = ebookWithCover[1];

                return ebookWithCover[0];
            })
            .catch(this.handleError);
    }

    create(ebook: IEbook): Observable<IEbook> {
        let body = JSON.stringify(ebook);
        let headers = new Headers({"Content-Type": "application/json"});
        let options = new RequestOptions({headers: headers});

        return this.xhr.post(this.ebooksApi, body, options)
            .map((response: Response) => response.json() || {})
            .catch(this.handleError);
    }

    createEbookAndCover(ebook: IEbook, ebookCover: File): Observable<IEbook> {
        return this.create(ebook)
            .flatMap((newEbook: IEbook) => {
                return this.updateCover(newEbook.id, ebookCover);
            })
            .catch(this.handleError);
    }

    update(ebook: IEbook): Observable<IEbook> {
        let url = `${this.ebooksApi}/${ebook.id}`;
        let body = JSON.stringify(ebook);
        let headers = new Headers({"Content-Type": "application/json"});
        let options = new RequestOptions({headers: headers});

        return this.xhr.put(url, body, options)
            .map((response: Response) => response.json() || {})
            .catch(this.handleError);
    }

    updateEbookAndCover(ebook: IEbook, ebookCover: File): Observable<IEbook> {
        return this.update(ebook)
            .flatMap(() =>  this.updateCover(ebook.id, ebookCover))
            .catch(this.handleError);
    }

    remove(id: number): Observable<Response> {
        let url = `${this.ebooksApi}/${id}`;

        return this.xhr.delete(url)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getCoverById(id: number): Observable<string> {
        let url = `${this.ebooksCoversApi}/${id}`;

        return Observable.create((observer: Observer<string>) => {
            observer.next(url);
            observer.complete();
        });
    }

    updateCover(id: number, file: File): Observable<any> {
        let url = `${this.ebooksCoversApi}/${id}`;
        let formData = new FormData();

        formData.append("file", file);

        return this.xhr.post(url, formData)
            .map((resonse: Response) => resonse)
            .catch(this.handleError);
    }

    getPublishers(): Observable<Array<string>> {
        return this.xhr.get(this.publishersApi)
            .map((response: Response) => response.json() || [])
            .catch(this.handleError);
    }

    getCurrencies(): Observable<Array<string>> {
        return this.xhr.get(this.currenciesApi)
            .map((response: Response) => response.json() || [])
            .catch(this.handleError);
    }

    private handleError(error: any): ErrorObservable {
        let message = (error.message)
            ? error.message
            : error.status ? `${error.status} - ${error.statusText}` : 'Server error';

        console.error(message);

        return Observable.throw(message);
    }
}
