import {Injectable } from "@angular/core";
import {Router, Resolve, ActivatedRouteSnapshot} from "@angular/router";
import {Observable } from "rxjs/Observable";

import {EbooksService} from "./ebooks.service";
import {IEbook} from "./../ebooks.interface";

@Injectable()
export class EbooksListResolve implements Resolve<IEbook> {
    constructor(private router: Router,
                private ebooksService: EbooksService) {}
    
    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        return this.ebooksService.get()
            .catch(() => this.router.navigate(["/error"]));
    }
}
