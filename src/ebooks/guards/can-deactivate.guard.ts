import {CanDeactivate} from "@angular/router";
import {Observable} from "rxjs/Observable"

import {EbooksDetailsComponent} from "./../ebooks-details/ebooks-details.component";

export class ConfirmDeactivateGuard implements CanDeactivate<EbooksDetailsComponent> {
    canDeactivate(component: EbooksDetailsComponent): Observable<boolean> | Promise<boolean> | boolean {
        return component.confirmCancel();
    }
}
