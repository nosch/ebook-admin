export interface IEbook {
    id: number;

    title: string;

    author: string;

    publisher: string;

    isbn: string;

    price: number;

    currency: string;

    coverUrl: string;

    coverData: string;
}
