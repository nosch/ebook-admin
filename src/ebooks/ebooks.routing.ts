import {ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

import {ConfirmDeactivateGuard} from "./guards/can-deactivate.guard";

import {EbooksListComponent}  from './ebooks-list/ebooks-list.component';
import {EbooksDetailsComponent}  from './ebooks-details/ebooks-details.component';

import {EbooksListResolve} from "./services/ebooks-list-resolve.service";
import {EbooksDetailsResolve} from "./services/ebooks-details-resolve.service";

const ebooksRoutes: Routes = [
    {
        path: '',
        redirectTo: '/ebooks',
        pathMatch: 'full'
    },
    {
        path: "ebooks",
        component: EbooksListComponent,
        resolve: {
            list: EbooksListResolve,
        }
    },
    {
        path: "ebooks/:id",
        component: EbooksDetailsComponent,
        canDeactivate: [ConfirmDeactivateGuard],
        data: {
            mode: "edit"
        },
        resolve: {
            details: EbooksDetailsResolve
        }
    },
    {
        path: "ebook-new",
        component: EbooksDetailsComponent,
        canDeactivate: [ConfirmDeactivateGuard],
        data: {
            mode: "create"
        },
        resolve: {
            details: EbooksDetailsResolve
        }
    }
];

export const ebooksRouting: ModuleWithProviders = RouterModule.forChild(ebooksRoutes);
