import {IEbook} from "./ebooks.interface";

export class Ebook implements IEbook {
    public id: number;
    public title: string;
    public author: string;
    public publisher: string;
    public isbn: string;
    public price: number;
    public currency: string;
    public coverUrl: string;
    public coverData: string;

    constructor(config: any) {
        this.id = config.id || 0;
        this.title = config.title || "";
        this.author = config.author || "";
        this.publisher = config.publisher || "";
        this.isbn = config.isbn || "";
        this.price = config.price || 0;
        this.currency = config.currency || "";
        this.coverUrl = config.coverUrl || "";
        this.coverData = config.coverData || "";
    }
}
