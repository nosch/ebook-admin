import {Component, Input, Output, EventEmitter, ViewChild, Renderer} from "@angular/core";

import {IEbook} from "./../ebooks.interface";

@Component({
    selector: "hgv-ebooks-covers",
    templateUrl: "./ebooks-covers.component.html",
    styles: [
        "section {margin-bottom: 10px}",
        "section > header {display: flex; align-items: center; justify-content: center; min-height: 150px}",
        ".mdl-card__supporting-text {display: flex; align-items: center; justify-content: center; min-height: inherit}",
        ".cover-label {color: rgb(63,81,181); font-size: 12px}",
        "input[type=file] {display: none}",
        ".file-type-error {color: rgb(213,0,0)}",
        "button {width: 100%}",
    ]
})
export class EbooksCoversComponent {
    @ViewChild("file") private fileInput: any;

    @Input() label: string;
    @Input() ebook: IEbook;
    @Output() onSelectFile = new EventEmitter<{fileData: File}>();
    @Output() onClearFileSelection = new EventEmitter<any>();

    public fileTypeError: boolean = false;

    private filePreview: any;
    private fileData: File;

    constructor(private renderer: Renderer) {}

    openFileSelection(): void {
        this.renderer.invokeElementMethod(this.fileInput.nativeElement, "click");
    }

    selectFile(): void {
        let file: File = this.fileInput.nativeElement.files[0]; // $event.target.files[0];
        let fileReader: any;

        if (!/\.(jpe?g)$/i.test(file.name)) {
            this.fileTypeError = true;
            return;
        }

        this.fileData = file;

        fileReader = new FileReader();

        fileReader.onloadend = (loadEvent: any) => {
            this.fileTypeError = false;
            this.filePreview = loadEvent.target.result;
            this.onSelectFile.emit({fileData: this.fileData});
        };

        if (file) {
            fileReader.readAsDataURL(file);
        }
    }

    clearFileSelection(): void {
        this.resetFileData();
        this.onClearFileSelection.emit(null);
    }

    private resetFileData(): void {
        this.fileData = null;
        this.filePreview = null;
        this.renderer.setElementProperty(this.fileInput.nativeElement, "value" , null);
    }
}
