import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ReactiveFormsModule} from "@angular/forms";

import {EbooksListComponent} from "./ebooks-list/ebooks-list.component";
import {EbooksDetailsComponent} from "./ebooks-details/ebooks-details.component";
import {EbooksCoversComponent} from "./ebooks-covers/ebooks-covers.component";

import {ConfirmDeactivateGuard} from "./guards/can-deactivate.guard";

import {EbooksService} from "./services/ebooks.service";
import {EbooksListResolve} from "./services/ebooks-list-resolve.service";
import {EbooksDetailsResolve} from "./services/ebooks-details-resolve.service";

import {ebooksRouting} from "./ebooks.routing";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ebooksRouting
    ],
    declarations: [
        EbooksListComponent,
        EbooksDetailsComponent,
        EbooksCoversComponent
    ],
    providers: [
        ConfirmDeactivateGuard,
        EbooksService,
        EbooksListResolve,
        EbooksDetailsResolve
    ]
})
export class EbooksModule {}
