import {Component} from "@angular/core";

@Component({
    selector: "hgv-contact",
    templateUrl: "./contact.component.html",
    styles: [".mdl-card {width: auto}"]
})
export class ContactComponent {}
