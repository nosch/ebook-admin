import {ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {ContactComponent}  from './../contact/contact.component';

const contactRoutes: Routes = [
    {path: "contact", component: ContactComponent}
];

export const contactRouting: ModuleWithProviders = RouterModule.forChild(contactRoutes);
