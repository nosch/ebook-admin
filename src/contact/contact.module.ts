import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {ContactComponent} from "./contact.component"

import {contactRouting} from "./contact.routing";

@NgModule({
    imports: [
        CommonModule,
        contactRouting
    ],
    declarations: [
        ContactComponent
    ],
    exports: [
        ContactComponent
    ],
    providers: []
})
export class ContactModule {}
