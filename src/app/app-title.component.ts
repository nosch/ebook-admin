import {Component, Input} from "@angular/core";

@Component({
    selector: "hgv-app-title",
    template: `
        <i class="material-icons">&#xE02F;</i>
        {{text}}
    `
})
export class AppTitleComponent {
    @Input() text: string;
}
