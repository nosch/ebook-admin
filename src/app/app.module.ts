import {NgModule} from "@angular/core";
import {BrowserModule}  from "@angular/platform-browser";
import {HttpModule} from '@angular/http';

import {AppModalService} from "./services/app-modal.service";
import {MdlLayoutDirective} from "./directives/mdl-layout.directive";

import {AppComponent} from "./app.component";
import {AppTitleComponent} from "./app-title.component";
import {AppNavComponent} from "./app-nav.component";

import {EbooksModule} from "./../ebooks/ebooks.module";
import {ErrorModule} from "./../error/error.module";
import {ContactModule} from "./../contact/contact.module";

import {routing, appRoutingProviders} from "./app.routing";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        routing,
        EbooksModule,
        ContactModule,
        ErrorModule
    ],
    declarations: [
        AppComponent,
        AppTitleComponent,
        AppNavComponent,
        MdlLayoutDirective
    ],
    providers: [
        appRoutingProviders,
        AppModalService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}
