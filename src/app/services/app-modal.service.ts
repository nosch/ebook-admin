import {Injectable} from "@angular/core";
import "sweetalert/dist/sweetalert.css";

type AlertType = "info" | "success" | "error";

@Injectable()
export class AppModalService {
    private win: any;

    constructor () {
        this.win = window;
    }

    prompt(options: any, onPrompt: (value: any) => void): any {
        const baseOptions = {
            showCancelButton: true,
            confirmButtonText: "Submit",
            type: "input"
        };

        this.win.swal(Object.assign(baseOptions, options), onPrompt);
    }

    confirm(options: any, onConfirm: (confirmed: boolean) => void): any {
        const baseOptions = {
            showCancelButton: true,
            confirmButtonText: "Confirm",
            type: "warning"
        };

        this.win.swal(Object.assign(baseOptions, options), onConfirm);
    }

    info(options: any): void {
        this.alert("info", options);
    }

    success(options: any): void {
        this.alert("success", options);
    }

    error(options: any): void {
        this.alert("error", options);
    }

    private alert(type: AlertType, options: any): void {
        const baseOptions = {
            type: type,
            confirmButtonText: "Ok",
        };

        this.win.swal(Object.assign(baseOptions, options));
    }
}
