import {Component, Input, Output, EventEmitter} from "@angular/core";

@Component({
    selector: "hgv-app-nav",
    styles: [
        "a {cursor: pointer}",
        "a.active {color: #8fbc8f}"
    ],
    template: `
        <a routerLink="ebook-new" 
           routerLinkActive="active"
           class="mdl-navigation__link"
           (click)="clickNavItem()">
           <i class="material-icons">&#xE148;</i> {{items.ebookNew}}
        </a>
        <a routerLink="ebooks" 
           routerLinkActive="active"
           class="mdl-navigation__link"
           (click)="clickNavItem()">
           <i class="material-icons">&#xE896;</i> {{items.ebooksList}}
        </a>
        <a routerLink="contact"
           routerLinkActive="active"
           [routerLinkActiveOptions]="{exact: true}"
           class="mdl-navigation__link" 
           (click)="clickNavItem()">
            <i class="material-icons">&#xE0E1;</i> {{items.contact}}
        </a>
    `
})
export class AppNavComponent {
    @Input() items: any;
    @Output() onClickNavItem = new EventEmitter<any>();

    clickNavItem(): void {
        this.onClickNavItem.emit(null);
    }
}
