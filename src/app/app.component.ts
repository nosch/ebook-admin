import {Component, ViewChild, Renderer} from "@angular/core";
import "./../../public/css/styles.css";

@Component({
    selector: "hgv-app",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"]
})
export class AppComponent {
    @ViewChild("layout") private mdlLayout: any;

    public appTitle: string = "eBook Admin";

    public navItems: any = {
        ebookNew: "Neues eBook",
        ebooksList: "Alle eBooks",
        contact: "Kontakt"
    };

    constructor(private renderer: Renderer) {}

    /**
     * Only used for the left menu drawer
     */
    toggleMenu(): void {
        this.renderer.invokeElementMethod(this.mdlLayout.nativeElement.MaterialLayout, "toggleDrawer");
    }
}
