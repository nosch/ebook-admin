// Angular 2
import "@angular/platform-browser";
import "@angular/platform-browser-dynamic";
import "@angular/core";
import "@angular/common";
import "@angular/http";
import "@angular/router";

// RxJS
import "rxjs";

// Material Design Lite
import "./../node_modules/material-design-lite/material.css";
import "./../node_modules/material-design-lite/material";
import "./../node_modules/material-design-icons/iconfont/material-icons.css";

// Material Design: selectfield
import "./../node_modules/mdl-selectfield/dist/mdl-selectfield.css";
import "./../node_modules/mdl-selectfield/dist/mdl-selectfield";

// SweetAlert
import "./../node_modules/sweetalert/dist/sweetalert.css";
import "./../node_modules/sweetalert/themes/google/google.css";
import "./../node_modules/sweetalert/lib/sweetalert";
