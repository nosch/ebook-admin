# eBook-Admin - eine Angular2 Web-Applikation (Probeaufgabe für HGV)

## Dev-Stack

1. Angular2 (v2.0.0)
2. TypeScript (v1.8.10)
3. Material Design Lite (v1.2.1)
4. Webpack (v1.13.2) - Module Bundler und Applikation-Server (Proxy für Zugriff auf HGV-API)

## Voraussetzungen für die Installation der Applikation

[node.js und npm](http://nodejs.org/en/download/ "Download node.js") installieren (Node 4.5 und NPM 2.15)

## Installation der Applikation

1. $ git clone https://bitbucket.org/nosch/ebook-admin.git "ebook-admin"
2. $ cd ebook-admin
3. $ npm install

## Start der Applikation

1. $ npm start
2. http://localhost:8080 im Browser öffnen

## Build der Applikation

1. $ npm run build
2. Build befindet sich dann in /dist
